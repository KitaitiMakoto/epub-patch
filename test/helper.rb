require 'rubygems'

begin
  require 'bundler/setup'
rescue LoadError => error
  abort error.message
end

require "simplecov"
SimpleCov.start do
  add_filter /test|vendor/
end

require 'test/unit'
require "pry"

class Test::Unit::TestCase
end
