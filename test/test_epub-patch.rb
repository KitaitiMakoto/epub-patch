require 'helper'
require 'epub/patch'

class TestEPUBPatch < Test::Unit::TestCase

  def test_version
    version = EPUB::Patch.const_get('VERSION')

    assert !version.empty?, 'should have a VERSION constant'
  end

end
