= EPUB Patch

Applies patches to EPUB files.

* link:https://rubygems.org/gems/epub-patch[Homepage]
* link:http://rubydoc.info/gems/epub-patch/frames[Documentation]
* link:mailto:KitaitiMakoto-at-gmail.com[Email]

== Description

EPUB::Patch applies patches from EPUB patch format files to EPUB files

== Features

== Examples

----
require 'epub/patch'
----

== Requirements

== Install

----
$ gem install epub-patch
----

== See also

* link:https://tools.ietf.org/html/rfc5261[RFC 5261 - An Extensible Markup Language (XML) Patch Operations Framework Utilizing XML Path Language (XPath) Selectors]
* link:https://tools.ietf.org/html/rfc6902[RFC 6902 - JavaScript Object Notation (JSON) Patch]
* link:https://tools.ietf.org/html/rfc7386[RFC 7386 - JSON Merge Patch]

== Copyright

Copyright (c) 2019 Kitaiti Makoto
